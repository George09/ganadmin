/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author usr-6891
 */
public class MetodoIT {
    
    public MetodoIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calcularPersonasTiempoParaArreglos method, of class Metodo.
     */
    @Test
    public void testCalcularPersonasTiempoParaArreglos() {
        System.out.println("calcularPersonasTiempoParaArreglos");
        int n = 0;
        Metodo instance = new Metodo();
        int[] expResult = null;
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        if(result == null){
            fail("fallo");
        }
    }
       @Test
    public void testCalcularPersonasTiempoParaArreglos2() {
        System.out.println("calcularPersonasTiempoParaArreglos");
        int n = 19;
        Metodo instance = new Metodo();
        int[] expResult = null;
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
           assertEquals("Es correcto", 1, result[0]);
        
        if(result == null){
            fail("fallo");
        }
}

       @Test
    public void testCalcularPersonasTiempoParaArreglos3() {
        System.out.println("calcularPersonasTiempoParaArreglos");
        int n = 49;
        Metodo instance = new Metodo();
        int[] expResult = null;
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
           assertEquals("Es correcto", 2, result[0]);
        
        if(result == null){
            fail("fallo");
        }
    }

       @Test
    public void testCalcularPersonasTiempoParaArreglos4() {
        System.out.println("calcularPersonasTiempoParaArreglos");
        int n = 50;
        Metodo instance = new Metodo();
        int[] expResult = null;
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
           assertEquals("Es correcto", 3, result[0]);
        
        if(result == null){
            fail("fallo");
        }
}
     @Test
    public void testCalcularPersonasTiempoParaArreglos5() {
        System.out.println("calcularPersonasTiempoParaArreglos");
        int n = 19;
        Metodo instance = new Metodo();
        int[] expResult = null;
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
           assertEquals("Es correcto", n*15, result[1]);
        
        if(result == null){
            fail("fallo");
        }
}
    @Test
    public void testCalcularPersonasTiempoParaArreglos6() {
        System.out.println("calcularPersonasTiempoParaArreglos");
        int n = 25;
        Metodo instance = new Metodo();
        int[] expResult = null;
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
           assertEquals("Es correcto", n*15, result[1]);
        
        if(result == null){
            fail("fallo");
        }
}
}
